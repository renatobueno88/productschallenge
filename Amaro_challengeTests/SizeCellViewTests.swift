//
//  SizeCellViewTests.swift
//  Amaro_challengeTests
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import Amaro_challenge

class SizeCellViewTests: XCTestCase {
    
    var sizeView: SizesCellView = SizesCellView.loadFromNib()
    var mockedSizes = [Sizes]()
    var sizePresenter = SizeCellPresenter(delegate: nil)
    
    func fetchMockedContent() {
        MockedProductsRequest().fetchProducts { (products, _) in
            guard let products = products, let singleProduct = products.last else {
                return
            }
            self.mockedSizes = singleProduct.sizes
        }
    }
    
    override func setUp() {
        super.setUp()
        UIApplication.shared.keyWindow?.addSubview(sizeView)
        XCTAssertNotNil(sizeView)
        fetchMockedContent()
        sizePresenter = SizeCellPresenter(delegate: sizeView)
        sizeView.presenter = sizePresenter
        sizeView.fill(sizes: mockedSizes, sizeDelegate: nil)
    }
    
    func testView() {
        guard let collectionView = sizeView.collectionView else {
            XCTFail()
            return
        }
        XCTAssertTrue(mockedSizes.allSatisfy({ $0.isAvailable }))
        XCTAssertEqual(sizeView.presenter.numberOfItems(inSection: 0), mockedSizes.count)
        XCTAssertEqual(sizeView.presenter.numberOfSections(), 1)
        
        XCTAssertEqual(collectionView.allowsMultipleSelection, false)
        XCTAssertEqual(collectionView.numberOfItems(inSection: 0), sizePresenter.numberOfItems(inSection: 0))
        XCTAssertEqual(collectionView.numberOfSections, sizePresenter.numberOfSections())
    }
    
}
