//
//  ProductListTests.swift
//  ProductListTests
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import Amaro_challenge

class ProductListTests: XCTestCase {

    var controller: ProductListViewController?
    var productsPresenter = ProductsPresenter(presenterDelegate: nil)
    var productsInteractor = ProductsInteractor(productsServiceType: MockedProductsRequest.self, interactorDelegate: nil)
    var mockedProducts = [Product]()
    
    func setupController() {
        let controllerIdentifer = String(describing: ProductListViewController.self)
        controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controllerIdentifer) as? ProductListViewController
        productsPresenter = ProductsPresenter(presenterDelegate: controller, productsServiceType: MockedProductsRequest.self)
        productsInteractor = ProductsInteractor(productsServiceType: MockedProductsRequest.self, interactorDelegate: productsPresenter)
        controller?.presenter = productsPresenter
    }
    
    func fetchMockedContent() {
        MockedProductsRequest().fetchProducts { (products, _) in
            self.mockedProducts = products ?? []
        }
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    override func tearDown() {

    }
   
    func testListControllerSetup() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        controller.presenter.loadContent()
        self.fetchMockedContent()
        XCTAssertEqual(controller.presenter.headerTitle, "Apenas produtos disponíveis:")
        XCTAssertEqual(controller.presenter.numberOfItems(inSection: 0), mockedProducts.count)
        XCTAssertEqual(controller.presenter.numberOfSections(), 1)
    }
    
    func testControllerItems() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        controller.presenter.loadContent()
        self.fetchMockedContent()
        XCTAssertEqual(controller.title, "Produtos")
        XCTAssertEqual(controller.collectionView.numberOfSections, 1)
        XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), mockedProducts.count)
    }

    func testItemDto() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        self.fetchMockedContent()
        var index = IndexPath(row: 0, section: 0)
        let formattedPrice = productsPresenter.getFormattedPrice(currentProduct: mockedProducts[index.row])
        let formattedDiscount = productsPresenter.getFormattedDiscountPrice(currentProduct: mockedProducts[index.row])
        let mockedItemDto = ItemDto(imagePath: "https://d3l7rqep7l31az.cloudfront.net/images/products/20002605_615_catalog_1.jpg?1460136912",
                                    name: "VESTIDO TRANSPASSE BOW",
                                    formattedPrice: formattedPrice,
                                    percentage: formattedDiscount?.percentage ?? "",
                                    oldPrice: formattedDiscount?.oldPrice)
        let itemDto = controller.presenter.getItemDTO(atIndex: index)
        XCTAssertEqual(itemDto.imagePath, mockedItemDto.imagePath)
        XCTAssertEqual(itemDto.name, mockedItemDto.name)
        XCTAssertEqual(itemDto.formattedPrice, mockedItemDto.formattedPrice)
        XCTAssertNil(itemDto.oldPrice)
        XCTAssertTrue(itemDto.percentage.isEmpty)
        XCTAssertEqual(itemDto.oldPrice, mockedItemDto.oldPrice)
        XCTAssertEqual(itemDto.percentage, mockedItemDto.percentage)
    }
    
    func testFilteredItemDto() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        self.fetchMockedContent()
        let index = IndexPath(row: 1, section: 0)
        controller.didTapSwitch(isOn: true)
        let mockedFilteredProducts = mockedProducts.filter({ $0.isOnSale })
        
        let itemDto = controller.presenter.getItemDTO(atIndex: index)
        let formattedPrice = productsPresenter.getFormattedPrice(currentProduct: mockedFilteredProducts[index.row])
        let formattedDiscount = productsPresenter.getFormattedDiscountPrice(currentProduct: mockedFilteredProducts[index.row])
        let mockedItemDto = ItemDto(imagePath: "https://d3l7rqep7l31az.cloudfront.net/images/products/20002945_027_catalog_1.jpg?1459540966",
                                name: "BOLSA FLAP TRIANGLE",
                                formattedPrice: formattedPrice,
                                percentage: formattedDiscount?.percentage ?? "",
                                oldPrice: formattedDiscount?.oldPrice)
        
        XCTAssertEqual(itemDto.imagePath, mockedItemDto.imagePath)
        XCTAssertEqual(itemDto.name, mockedItemDto.name)
        XCTAssertEqual(itemDto.formattedPrice, mockedItemDto.formattedPrice)
        XCTAssertNotNil(itemDto.oldPrice)
        XCTAssertFalse(itemDto.percentage.isEmpty)
        XCTAssertEqual(itemDto.oldPrice, mockedItemDto.oldPrice)
        XCTAssertEqual(itemDto.percentage, mockedItemDto.percentage)
    }
    
    func testFilteredResults() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        fetchMockedContent()
        productsPresenter.loadFilteredContent(onlyOnSale: true)
        let mockedOnSale = mockedProducts.filter({ $0.isOnSale })
        XCTAssertEqual(controller.collectionView.numberOfItems(inSection: 0), mockedOnSale.count)
    }
    
    func testProductDetailEntity() {
        let index = IndexPath(row: 2, section: 0)
        productsPresenter.loadContent()
        fetchMockedContent()
        guard let selectedProduct = productsPresenter.getSelectedProduct(atIndex: index) else {
            XCTFail()
            return
        }
        let formattedPrice = productsPresenter.getFormattedPrice(currentProduct: mockedProducts[index.row])
        let formattedDiscount = productsPresenter.getFormattedDiscountPrice(currentProduct: mockedProducts[index.row])
        XCTAssertEqual(selectedProduct.productName, "TOP CROPPED SUEDE")
        XCTAssertEqual(selectedProduct.currentPrice, "R$ 129,90")
        XCTAssertEqual(selectedProduct.formattedDiscount?.oldPrice, formattedDiscount?.oldPrice)
        XCTAssertEqual(selectedProduct.formattedDiscount?.percentage, formattedDiscount?.percentage)
        XCTAssertEqual(selectedProduct.priceInstallments, formattedPrice)
        XCTAssertEqual(selectedProduct.productImage, "https://d3l7rqep7l31az.cloudfront.net/images/products/20002575_027_catalog_1.jpg?1459537946")
        XCTAssertTrue(selectedProduct.sizes.allSatisfy({ $0.isAvailable }))
        XCTAssertEqual(selectedProduct.sizes.first?.sku, "5733_1000054_0_P")
        XCTAssertEqual(selectedProduct.sizes.first?.isAvailable, true)
        XCTAssertEqual(selectedProduct.sizes.first?.size, "P")
    }
    
}
