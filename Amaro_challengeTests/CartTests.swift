//
//  CartTests.swift
//  Amaro_challengeTests
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import Amaro_challenge

class CartTests: XCTestCase {
    
    var controller: CartViewController?
    var cartPresenter = CartPresenter()
    var cartItem = [CartItem]()
    
    func setupController() {
        controller = CartControllerBuilder().build() as? CartViewController
        cartPresenter = CartPresenter(cartLocalService: CartMockedLocalServiceRequest.self)
        controller?.presenter = cartPresenter
        fetchMockedContent()
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    override func tearDown() {
        super.tearDown()
        cartPresenter.cleanCart()
    }
    
    func fetchMockedContent() {
        MockedProductsRequest().fetchProducts { (products, _) in
            guard let products = products, let singleProduct = products.last else {
                return
            }
            let cart = CartItem(productName: singleProduct.name,
                                productImage: singleProduct.image,
                                currentPrice: singleProduct.actualPrice,
                                priceInstallments: singleProduct.installments,
                                selectedSize: singleProduct.sizes.first ?? Sizes())
            self.cartItem.append(cart)
        }
    }
    
    func testEmptyCart() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        cartPresenter.cleanCart()
        let index = IndexPath(row: 0, section: 0)
        XCTAssertNotNil(controller.tableView)
        XCTAssertEqual(cartPresenter.canEditRowAtIndex(index: index), false)
        XCTAssertEqual(cartPresenter.numberOfRows(inSection: index.section), 0)
        XCTAssertEqual(cartPresenter.getCartTitle(), "Carrinho (0)")
        XCTAssertEqual(controller.title, "Carrinho (0)")
        XCTAssertEqual(controller.tableView.numberOfRows(inSection: index.section), 0)
        XCTAssertEqual(controller.purchaseButton.isEnabled, false)
        XCTAssertEqual(controller.totalValueLabel.text, "Total: R$ 0,00")
    }
    
    func testCartPresenter() {
        guard let item = cartItem.first else {
            return
        }
        let index = IndexPath(row: 0, section: 0)
        cartPresenter.saveLocalCart(cart: item)
        cartPresenter.fetchLocalCart()
        XCTAssertEqual(cartPresenter.canEditRowAtIndex(index: index), true)
        XCTAssertEqual(cartPresenter.getCartTitle(), "Carrinho (1)")
        XCTAssertEqual(cartPresenter.canFinishPurchase(), true)
        XCTAssertEqual(cartPresenter.estimatedHeightForCell(indexPath: index), 170)
        XCTAssertEqual(cartPresenter.numberOfRows(inSection: index.section), 1)
        XCTAssertEqual(cartPresenter.numberOfSections(), 1)
        XCTAssertEqual(cartPresenter.getTotalPrice().string, "Total: R$ 29,90")
        let text = "Total: R$ 29,90"
        let priceAttributed = cartPresenter.createAttributedString(text: text, boldText: "R$ 29,90", fontSize: 16)
        XCTAssertEqual(cartPresenter.getTotalPrice(), priceAttributed)
    }
    
}
