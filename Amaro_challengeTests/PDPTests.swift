//
//  PDPTests.swift
//  Amaro_challengeTests
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import Amaro_challenge

class PDPTests: XCTestCase, PriceFormattedProtocol {
    
    var controller: PDPViewController?
    var pdpPresenter = PDPPresenter(delegate: nil)
    var mockedProductDetail = DetailProduct()
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
        XCTAssertNotNil(controller?.tableView)
    }
    
    func fetchMockedContent() {
        MockedProductsRequest().fetchProducts { (products, _) in
            guard let products = products, let singleProduct = products.last else {
                return
            }
            let price = self.getFormattedPrice(currentProduct: singleProduct)
            let discounts = self.getFormattedDiscountPrice(currentProduct: singleProduct)
            self.mockedProductDetail = DetailProduct(productName: singleProduct.name,
                                                     productImage: singleProduct.image,
                                                     currentPrice: singleProduct.actualPrice,
                                                     priceInstallments: price,
                                                     formattedDiscount: discounts,
                                                     sizes: singleProduct.sizes)
        }
    }
    
    func setupController() {
        fetchMockedContent()
        guard let pdpController = PDPControllerBuilder(transporter: Transporter(data: mockedProductDetail)).build() as? PDPViewController else {
            XCTFail()
            return
        }
        controller = pdpController
        pdpPresenter = PDPPresenter(delegate: controller)
        controller?.presenter = pdpPresenter
    }
    
    func testCellDataSetup() {
        pdpPresenter.transporter(object: Transporter(data: mockedProductDetail))
        XCTAssertEqual(pdpPresenter.getProductImage().imagePath, "https://d3l7rqep7l31az.cloudfront.net/images/products/20001913_009_catalog_1.jpg?")
        XCTAssertEqual(pdpPresenter.getProductNamePrice().productName, "PULSEIRA STYLISH")
        XCTAssertEqual(pdpPresenter.getProductNamePrice().price, mockedProductDetail.priceInstallments)
        XCTAssertTrue(pdpPresenter.getSizes.allSatisfy({ $0.isAvailable }))
        XCTAssertEqual(pdpPresenter.getProductImage().placeholder, UIImage(named: "Amaro-logo-novo"))
    }
    
    func testPresenterCellTypeSetup() {
        pdpPresenter.transporter(object: Transporter(data: mockedProductDetail))
        XCTAssertEqual(pdpPresenter.getCellType(fromIndex: IndexPath(row: 0, section: 0)), .image)
        XCTAssertEqual(pdpPresenter.getCellType(fromIndex: IndexPath(row: 1, section: 0)), .namePrice)
        XCTAssertEqual(pdpPresenter.getCellType(fromIndex: IndexPath(row: 2, section: 0)), .promotionalPrice)
        XCTAssertEqual(pdpPresenter.getCellType(fromIndex: IndexPath(row: 3, section: 0)), .sizes)
        XCTAssertEqual(pdpPresenter.numberOfSections(), 1)
        XCTAssertEqual(pdpPresenter.numberOfRows(inSection: 0), 4)
        XCTAssertEqual(pdpPresenter.cellTypes, PDPCellType.allCases)
        
        for i in 0...2 {
            XCTAssertEqual(pdpPresenter.estimatedHeightForCell(indexPath: IndexPath(row: i, section: 0)), UITableView.automaticDimension)
        }
        XCTAssertEqual(pdpPresenter.estimatedHeightForCell(indexPath: IndexPath(row: 3, section: 0)), 68)
    }
    
}



