# productsChallenge

iOS project with Swift 4.2, Xcode 10.1 and Cocoapods with the following pods: 

- Alamofire (4.8.0)
- Kingfisher (5.0.1)
- ObjectMapper (3.4.2)