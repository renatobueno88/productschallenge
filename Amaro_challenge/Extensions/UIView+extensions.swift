//
//  UIView+extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol CalculateCellSizeProtocol {
    func calculateSizeCell(numberOfCells: Float, cellSpace: CGFloat?) -> CGSize
}

extension UIView: CalculateCellSizeProtocol {
    func calculateSizeCell(numberOfCells: Float, cellSpace: CGFloat?) -> CGSize {
        let screenValueToCalculate = 0.9465240642
        var viewWidth = self.frame.size.width
        if let cellSpace = cellSpace {
            viewWidth -= cellSpace
        }
        let width = viewWidth > 0 ? Double(viewWidth / CGFloat(numberOfCells)) : Double(1 / CGFloat(numberOfCells))
        
        let height = Double(width * screenValueToCalculate + 105)
        return CGSize(width: width, height: height)
    }
}

extension UIView {
    
    func circleView(withBackgroundColor: UIColor? = nil) {
        if let bgColor = withBackgroundColor {
            self.backgroundColor = bgColor
        }
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)!.first as! T
    }
    
    func fillWithSubview(subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
        
        let views = ["view": subview]
        let vC = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views)
        let hC = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views)
        
        addConstraints(vC)
        addConstraints(hC)
        updateConstraints()
        
        subview.setNeedsDisplay()
        layoutIfNeeded()
    }
    
}
