//
//  NSAttributedString+extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol AttributedStringProtocol {
    func createAttributedString(text: String, boldText: String, fontSize: CGFloat, withColor: UIColor?, boldColor: UIColor?) -> NSAttributedString
}

extension AttributedStringProtocol {
    func createAttributedString(text: String, boldText: String, fontSize: CGFloat, withColor: UIColor? = nil, boldColor: UIColor? = nil) -> NSAttributedString {
        let attributed = NSMutableAttributedString(string: text)
        attributed.addFont(string: text, font: .systemFont(ofSize: fontSize))
        attributed.addFont(string: boldText, font: .boldSystemFont(ofSize: fontSize))
        attributed.addColor(string: text, color: withColor ?? .black)
        attributed.addColor(string: boldText, color: boldColor ?? .black)
        return attributed
    }
}

extension NSMutableAttributedString {
    
    func addColor(string: String, color: UIColor) {
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: self.mutableString.range(of: string))
    }
    
    func addFont(string: String, font: UIFont) {
        self.addAttribute(NSAttributedString.Key.font, value: font, range: self.mutableString.range(of: string))
    }
    
}
