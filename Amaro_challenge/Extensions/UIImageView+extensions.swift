//
//  UIImageView+extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func downloadImage(url: String, placeholder: UIImage? = nil) {
        guard !url.isEmpty, let validURL = URL(string: url), UIApplication.shared.canOpenURL(validURL) else {
            self.image = placeholder
            return
        }
        kf.setImage(with: validURL)
    }
}
