//
//  UIViewController+Extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showDefaultSystemAlert(message: String, actionTitle: String = "Tentar novamente", cancelTitle: String = "Cancelar", okActionHandler: ((UIAlertAction) -> Void)?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alerta", message: message, preferredStyle: .alert)
            if !cancelTitle.isEmpty {
                let dismissAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
                alert.addAction(dismissAction)
            }
            if !actionTitle.isEmpty {
                let action = UIAlertAction(title: actionTitle, style: .default, handler: okActionHandler)
                alert.addAction(action)
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func addLeftCancelBackButton() {
        let backButton = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(self.cancelController))
        backButton.tintColor = .black
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func cancelController() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupCartOnNavigation() -> UIBarButtonItem {
        let cartImage = UIImage(named: "shopping-cart")
        let cartItem = UIBarButtonItem(image: cartImage,
                                       style: .plain,
                                       target: self,
                                       action: #selector(addToCartAction))
        return cartItem
    }
    
    @objc func addToCartAction() { }
}


