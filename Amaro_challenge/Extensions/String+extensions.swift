//
//  String+extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension String {
    var dashedAttributedString: NSAttributedString {
        let attributes = [NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue)]
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    func removeCurrency() -> Double {
        let formatter = NumberFormatter()
        let locale = Locale(identifier: "pt_BR")
        formatter.locale = locale
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = locale.currencySymbol ?? "R$"
        formatter.decimalSeparator = locale.groupingSeparator ?? ","
        let value = self.replacingOccurrences(of: " ", with: "")
        return formatter.number(from: value)?.doubleValue ?? 0.00
    }
    
    func addingCurrencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "pt_BR")
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
}
