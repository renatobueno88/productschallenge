//
//  Cell+Extensions.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class func createCell<T: UITableViewCell>(tableView: UITableView, indexPath: IndexPath) -> T {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}

extension UITableView {
    func registerCell(identifier: String) {
        self.register(UINib(nibName: identifier, bundle: Bundle.main), forCellReuseIdentifier: identifier)
    }
    
    func animateCellExibition(atIndex indexPath: IndexPath, cell: UITableViewCell, duration: TimeInterval = 0.5) {
        cell.alpha = 0
        UIView.animate(withDuration: duration, delay: (0.3 * Double(indexPath.row)), animations: {
            cell.alpha = 1
        })
    }
}

extension UICollectionViewCell {
    class func createCell<T: UICollectionViewCell>(collectionView: UICollectionView, indexPath: IndexPath) -> T {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}

extension UICollectionView {
    func registerCell(identifier: String) {
        self.register(UINib(nibName: identifier, bundle: Bundle.main), forCellWithReuseIdentifier: identifier)
    }
}
