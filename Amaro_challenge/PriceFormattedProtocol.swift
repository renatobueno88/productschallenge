//
//  PriceFormattedProtocol.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol PriceFormattedProtocol: AttributedStringProtocol {
    func getFormattedPrice(currentProduct product: Product) -> NSAttributedString
    func getFormattedDiscountPrice(currentProduct product: Product) -> (percentage: String, oldPrice: NSAttributedString?)?
}

extension PriceFormattedProtocol {
    
    func getFormattedPrice(currentProduct product: Product) -> NSAttributedString {
        let text = "\(product.actualPrice) \(product.installments)"
        let boldText = product.actualPrice
        return createAttributedString(text: text, boldText: boldText, fontSize: 14, withColor: .darkGray, boldColor: .black)
    }
    
    func getFormattedDiscountPrice(currentProduct product: Product) -> (percentage: String, oldPrice: NSAttributedString?)? {
        if product.discountPercentage.isEmpty && product.actualPrice.isEmpty {
            return nil
        } else if product.actualPrice == product.regularPrice {
                return (percentage: product.discountPercentage, oldPrice: nil)
        } else {
            let oldPrice = product.actualPrice.isEmpty ? nil : product.actualPrice.dashedAttributedString
            return (percentage: product.discountPercentage, oldPrice: oldPrice)
        }
    }
}
