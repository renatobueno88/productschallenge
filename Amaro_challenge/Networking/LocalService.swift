//
//  LocalService.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol LocalService {
    associatedtype LocalObject
    func saveObject(withKey key: String, object: LocalObject)
    func queryObject(withKey key: String) -> LocalObject?
    func removeObject(withKey key: String)
}
