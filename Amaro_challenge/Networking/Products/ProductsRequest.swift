//
//  ProductsRequest.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class ProductsRequest: ProductsService {
    
    private var requestLoaderService: RequestableProtocol {
        return AlamoFireLoader.init()
    }
    
    required init() {
    }
    
    func fetchProducts(completion: @escaping ([Product]?, Error?) -> ()) {
        requestLoaderService.requestJSON(method: .get) { (response) in
            guard let result = response.result.value as? JSONBody, let jsonProducts = result["products"] else {
                completion(nil, response.result.error)
                return
            }
            let product = (jsonProducts as? [JSONBody])?.compactMap({ Product(JSON: $0) })
            completion(product, nil)
        }
    }
    
}
