//
//  ProductsService.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ProductsService {
    init()
    func fetchProducts(completion: @escaping ([Product]?, Error?) -> ())
}
