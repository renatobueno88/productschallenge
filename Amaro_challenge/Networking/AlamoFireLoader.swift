//
//  AlamoFireLoader.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSONBody = [String: Any]

protocol RequestableProtocol {
    init()
    func requestJSON(method: HTTPMethod, completionHandler: @escaping (DataResponse<Any>) -> Void)
}

struct Environment {
    static var baseUrl = "http://www.mocky.io/v2/59b6a65a0f0000e90471257d"
}

class AlamoFireLoader: RequestableProtocol {
    
    required init() {
    }
    
    func requestJSON(method: HTTPMethod, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        let baseURL = Environment.baseUrl
        guard let url = URL(string: baseURL) else {
            return
        }
        
        Alamofire.request(url, method: method)
            .validate()
            .responseJSON(completionHandler: completionHandler)
    }
}
