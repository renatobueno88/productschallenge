//
//  CartLocalServiceRequest.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol CartLocalServiceProtocol {
    init(key: String)
    func saveLocalObject(cartObject: CartItem)
    func removeObject()
    func removeSingleObject(object: CartItem)
    func queryObject() -> [CartItem]
}

class CartLocalServiceRequest: LocalService, CartLocalServiceProtocol {
    
    typealias LocalObject = CartItem
    private var key = ""
    
    required init(key: String) {
        self.key = key
    }
    
    static private var cart = [CartItem]()
    
    // MARK: CartLocalServiceProtocol
    
    func saveLocalObject(cartObject: CartItem) {
        self.saveObject(withKey: key, object: cartObject)
    }
    
    func removeObject() {
        self.removeObject(withKey: key)
    }
    
    func removeSingleObject(object: CartItem) {
        if let index = CartLocalServiceRequest.cart.index(where: { $0.selectedSize.sku == object.selectedSize.sku }) {
            CartLocalServiceRequest.cart.remove(at: index)
            removeObject()
        }
    }
    
    func queryObject() -> [CartItem] {
        guard queryObject(withKey: key) != nil else {
            return CartLocalServiceRequest.cart
        }
        return CartLocalServiceRequest.cart
    }
    
    // MARK: LocalService
    
    func saveObject(withKey key: String, object: CartItem) {
        let encoder = PropertyListEncoder()
        if let encoded = try? encoder.encode(object) {
            CartLocalServiceRequest.cart.append(object)
            UserDefaults.standard.set(encoded, forKey: key)
        }
    }
    
    func removeObject(withKey key: String) {
        guard queryObject(withKey: key) != nil else {
            return
        }
        CartLocalServiceRequest.cart.removeAll()
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    func queryObject(withKey key: String) -> CartItem? {
        if let data = UserDefaults.standard.data(forKey: key) {
            let decoder = PropertyListDecoder()
            if let object = try? decoder.decode(LocalObject.self, from: data) {
                return object
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
}
