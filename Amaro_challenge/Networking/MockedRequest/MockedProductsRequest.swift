//
//  MockedProductsRequest.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 13/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import ObjectMapper

class MockedProductsRequest: ProductsService {
    
    required init() {
    }
    
    func fetchProducts(completion: @escaping ([Product]?, Error?) -> ()) {
        let json = buildObject(resource: "products")
        let products = json.compactMap({ Product(JSON: $0) })
        completion(products, nil)
    }
    
    private func buildObject(resource: String, type: String = "json") -> [JSONBody] {
        let path = Bundle.main.path(forResource: resource, ofType: type)
        if path == nil {
            fatalError(".json file doesnt exist")
        }
        let url = URL(fileURLWithPath: path!)
        let data = try! Data(contentsOf: url)
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! JSONBody
        let jsonProducts = jsonResult["products"] as! [JSONBody]
        return jsonProducts
    }
}
