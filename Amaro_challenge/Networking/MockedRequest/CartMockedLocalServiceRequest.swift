//
//  CartMockedLocalServiceRequest.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class CartMockedLocalServiceRequest: LocalService, CartLocalServiceProtocol {
    
    typealias LocalObject = CartItem
    
    private var key = ""
    private static var cart = [CartItem]()
    
    required init(key: String) {
        self.key = key
    }
    
    // MARK: CartLocalServiceProtocol
    
    func saveLocalObject(cartObject: CartItem) {
        self.saveObject(withKey: key, object: cartObject)
    }
    
    func removeObject() {
        self.removeObject(withKey: key)
    }
    
    func removeSingleObject(object: CartItem) {
        self.removeObject()
    }
    
    func queryObject() -> [CartItem] {
        return CartMockedLocalServiceRequest.cart
    }
    
    // MARK: LocalService
    
    func saveObject(withKey key: String, object: CartItem) {
        CartMockedLocalServiceRequest.cart.append(object)
    }
    
    func removeObject(withKey key: String) {
        guard queryObject(withKey: key) != nil else {
            return
        }
        CartMockedLocalServiceRequest.cart.removeAll()
    }
    
    func queryObject(withKey key: String) -> CartItem? {
        return CartMockedLocalServiceRequest.cart.first
    }
    
}
