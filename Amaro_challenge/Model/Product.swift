//
//  Product.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import ObjectMapper

struct Product: Mappable {

    var name = ""
    var style = ""
    var colorCode = ""
    var colorSlug = ""
    var color = ""
    var isOnSale = false
    var regularPrice = ""
    var actualPrice = ""
    var discountPercentage = ""
    var installments = ""
    var image = ""
    var sizes = [Sizes]()
    
    init() {
    }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        style <- map["style"]
        colorCode <- map["code_color"]
        colorSlug <- map["color_slug"]
        color <- map["color"]
        isOnSale <- map["on_sale"]
        regularPrice <- map["regular_price"]
        actualPrice <- map["actual_price"]
        discountPercentage <- map["discount_percentage"]
        installments <- map["installments"]
        image <- map["image"]
        sizes <- map["sizes"]
    }
}
