//
//  DetailProduct.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ProductSetupProtocol {
    var productName: String { get set }
    var productImage: String { get set }
    var currentPrice: String { get set }
}

struct DetailProduct: ProductSetupProtocol {
    var productName = ""
    var productImage = ""
    var currentPrice = ""
    var priceInstallments = NSAttributedString(string: "")
    var formattedDiscount: (percentage: String, oldPrice: NSAttributedString?)?
    var sizes = [Sizes]()
}
