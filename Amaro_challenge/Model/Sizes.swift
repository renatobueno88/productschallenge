//
//  Sizes.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import ObjectMapper

struct Sizes: Mappable, Codable {
    
    var isAvailable = false
    var size = ""
    var sku = ""
    
    init() {
    }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        isAvailable <- map["available"]
        size <- map["size"]
        sku <- map["sku"]
    }
    
}
