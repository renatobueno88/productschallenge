//
//  CartItem.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct CartItem: ProductSetupProtocol, Codable, Equatable {
    var productName = ""
    var productImage = ""
    var currentPrice = ""
    var priceInstallments = ""
    var selectedSize = Sizes()
    
    static func == (lhs: CartItem, rhs: CartItem) -> Bool {
        return lhs.productName == rhs.productImage &&
               lhs.productImage == rhs.productImage &&
               lhs.currentPrice == rhs.currentPrice &&
               lhs.priceInstallments == rhs.priceInstallments &&
               lhs.selectedSize.sku == rhs.selectedSize.sku
    }
    
}
