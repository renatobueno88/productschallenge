//
//  CartViewController.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CartViewController: UIViewController, TableViewProtocols {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var cartResumeStackView: UIStackView!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var purchaseButton: UIButton!
    
    lazy var presenter = CartPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        presenter.fetchLocalCart()
        setupCartQuantityOnNavigation()
        addLeftCancelBackButton()
        setupResumeContent()
        tableView.reloadData()
    }
    
    func setupCartQuantityOnNavigation() {
        self.title = presenter.getCartTitle()
    }
    
    func setupResumeContent() {
        totalValueLabel.attributedText = presenter.getTotalPrice()
        purchaseButton.isEnabled = presenter.canFinishPurchase()
    }
    
    func createProductCell(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CartTableViewCell = CartTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(dto: presenter.setupProductCell(atIndex: indexPath))
        return cell
    }
    
    // MARK: TableViewProtocols
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return createProductCell(tableView: tableView, cellForRowAt: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.animateCellExibition(atIndex: indexPath, cell: cell)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let removalTitle = "Excluir"
        let deleteAction = UITableViewRowAction(style: .destructive, title: removalTitle) { action, indexPath in
            if action.title == removalTitle {
                self.presenter.removeItemInCart(atIndex: indexPath)
                self.setupCartQuantityOnNavigation()
                self.setupResumeContent()
                tableView.reloadData()
            }
        }
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return presenter.canEditRowAtIndex(index: indexPath)
    }
    
    // MARK: IBAction
    
    @IBAction func purchaseActionButton(_ sender: Any) {
        self.showDefaultSystemAlert(message: "Compra concluída!", actionTitle: "OK", cancelTitle: "")  { [unowned self] _ in
            self.presenter.cleanCart()
            self.dismiss(animated: true, completion: nil)
        }
    }
}
