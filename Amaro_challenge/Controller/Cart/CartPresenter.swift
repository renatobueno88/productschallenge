//
//  CartPresenter.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CartPresenter: CellSetupProtocol, AttributedStringProtocol {
    
    private var cartLocalServiceType: CartLocalServiceProtocol.Type
    private var cartLocalService: CartLocalServiceProtocol {
        return cartLocalServiceType.init(key: "UserCart")
    }
    private var cart: [CartItem]?
    
    init(cartLocalService: CartLocalServiceProtocol.Type = CartLocalServiceRequest.self) {
        self.cartLocalServiceType = cartLocalService
    }
    
    func getCartTitle() -> String {
        return "Carrinho (\(cart?.count ?? 0))"
    }
    
    func saveLocalCart(cart: CartItem) {
        cartLocalService.saveLocalObject(cartObject: cart)
    }
    
    func fetchLocalCart() {
        cart = cartLocalService.queryObject()
    }
    
    func removeItemInCart(atIndex indexPath: IndexPath) {
        guard let cart = cart, !cart.isEmpty else {
            return
        }
        self.cart?.remove(at: indexPath.row)
        cartLocalService.removeSingleObject(object: cart[indexPath.row])
    }
    
    func cleanCart() {
        cart?.removeAll()
        cartLocalService.removeObject()
    }
    
    func getTotalPrice() -> NSAttributedString {
        let totalValue = cart?.compactMap({ $0.currentPrice.removeCurrency() }).reduce(0, +) ?? 0
        let currencyFormattedValue = String(totalValue).addingCurrencyFormatting()
        let text = "Total: \(currencyFormattedValue)"
        return createAttributedString(text: text, boldText: currencyFormattedValue, fontSize: 16)
    }
    
    func setupProductCell(atIndex indexPath: IndexPath) -> CartProductDto {
        guard let cart = cart else {
            return CartProductDto()
        }
        let cartItem = cart[indexPath.row]
        return CartProductDto(productName: cartItem.productName,
                              productImage: cartItem.productImage,
                              currentPrice: cartItem.currentPrice,
                              installments: cartItem.priceInstallments,
                              selectedSize: "Tamanho: \(cartItem.selectedSize.size)",
                              sku: "SKU: \(cartItem.selectedSize.sku)")
    }
    
    func canFinishPurchase() -> Bool {
        guard let cart = cart else {
            return false
        }
        return !cart.isEmpty
    }
    
    func canEditRowAtIndex(index: IndexPath) -> Bool {
        guard let cart = cart else {
            return false
        }
        return !cart.isEmpty
    }
    
    // MARK: CellSetupProtocol
    
    func numberOfSections() -> Int {
        return cart != nil ? 1 : 0
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return cart?.count ?? 0
    }
    
    func estimatedHeightForCell(indexPath: IndexPath) -> CGFloat {
        return cart != nil ? 170 : .leastNonzeroMagnitude
    }
    
}

