//
//  CartCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct CartProductDto: ProductSetupProtocol {
    var productName = ""
    var productImage = ""
    var currentPrice = ""
    var installments = ""
    var selectedSize = ""
    var sku = ""
}

class CartCellView: UIView {
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var currentSizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func fill(dto: CartProductDto) {
        productNameLabel.text = dto.productName
        skuLabel.text = dto.sku
        currentSizeLabel.text = dto.selectedSize
        priceLabel.text = dto.currentPrice
        let placeholderImage = UIImage(named: "Amaro-logo-novo")
        productImageView.downloadImage(url: dto.productImage, placeholder: placeholderImage)
    }
    
}
