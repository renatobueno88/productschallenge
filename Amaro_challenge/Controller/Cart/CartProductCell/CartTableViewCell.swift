//
//  CartTableViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    let view: CartCellView = CartCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func fill(dto: CartProductDto) {
        view.fill(dto: dto)
    }

}
