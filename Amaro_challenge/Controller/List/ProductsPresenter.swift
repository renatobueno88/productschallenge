//
//  ProductsPresenter.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ProductsPresenterDelegate: class {
    func shouldShowError(withMessage message: String)
    func didFinishRequest()
}

class ProductsPresenter: ProductsInteractorDelegate, PriceFormattedProtocol {
    
    private weak var presenterDelegate: ProductsPresenterDelegate?
    private var interactor: ProductsInteractorProtocol?
    private var products = [Product]()
    private var nonFilteredProducts = [Product]()
    
    init(presenterDelegate: ProductsPresenterDelegate?, productsServiceType: ProductsService.Type = ProductsRequest.self) {
        self.presenterDelegate = presenterDelegate
        self.interactor = ProductsInteractor(productsServiceType: productsServiceType, interactorDelegate: self)
    }
    
    func loadContent() {
        interactor?.fetchProducts()
    }
    
    func loadFilteredContent(onlyOnSale: Bool) {
        let onSaleProducts = products.filter({ $0.isOnSale == onlyOnSale })
        self.products = onlyOnSale ? onSaleProducts : nonFilteredProducts
        presenterDelegate?.didFinishRequest()
    }
    
    func getSelectedProduct(atIndex indexPath: IndexPath) -> DetailProduct? {
        guard !products.isEmpty else {
            return nil
        }
        let product = products[indexPath.row]
        let discountPrice = getFormattedDiscountPrice(currentProduct: product)
        let formmatedPrice = getFormattedPrice(currentProduct: product)
        return DetailProduct(productName: product.name,
                             productImage: product.image,
                             currentPrice: product.actualPrice,
                             priceInstallments: formmatedPrice,
                             formattedDiscount: discountPrice,
                             sizes: product.sizes.filter({ $0.isAvailable }))
    }
    
    // MARK: ProductsInteractorDelegate
    
    func didFinishRequest(products: [Product]) {
        self.products = products
        nonFilteredProducts = products
        presenterDelegate?.didFinishRequest()
    }
    
    func shouldShowError(error: Error?) {
        let errorMessage = error?.localizedDescription ?? "Erro inesperado. Tente Novamente."
        presenterDelegate?.shouldShowError(withMessage: errorMessage)
    }
    
    // MARK: Cell Setup
    
    func numberOfSections() -> Int {
        return products.isEmpty ? 0 : 1
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return products.count
    }
    
    func getItemDTO(atIndex indexPath: IndexPath) -> ItemDto {
        guard !products.isEmpty else {
            return ItemDto()
        }
        let product = products[indexPath.row]
        let discountPrice = getFormattedDiscountPrice(currentProduct: product)
        let formmatedPrice = getFormattedPrice(currentProduct: product)
        return ItemDto(imagePath: product.image,
                       name: product.name,
                       formattedPrice: formmatedPrice,
                       percentage: discountPrice?.percentage ?? "",
                       oldPrice: discountPrice?.oldPrice)
    }
    
    var headerTitle: String {
        return "Apenas produtos disponíveis:"
    }
}
