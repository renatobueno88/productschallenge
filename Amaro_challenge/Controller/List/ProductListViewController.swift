//
//  ProductListViewController.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

public typealias CollectionViewProtocols = UICollectionViewDelegate & UICollectionViewDataSource

class ProductListViewController: UIViewController, UICollectionViewDelegateFlowLayout, CollectionViewProtocols, ProductsPresenterDelegate, FilterSwitchDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var pullToRefresh = UIRefreshControl()
    lazy var presenter = ProductsPresenter(presenterDelegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        setupPulltoRefresh()
        presenter.loadContent()
        self.title = "Produtos"
    }
    
    // MARK: PullToRefresh
    
    private func setupPulltoRefresh() {
        collectionView.refreshControl = pullToRefresh
        pullToRefresh.addTarget(self, action: #selector(ProductListViewController.reloadContent), for: .valueChanged)
    }
    
    @objc func reloadContent() {
        presenter.loadContent()
    }
    
    // MARK: Cell Setup
    
    func createItemCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ItemCollectionViewCell = ItemCollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        cell.fill(dto: presenter.getItemDTO(atIndex: indexPath))
        return cell
    }
    
    func createFilterHeaderView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var headerView = FilterHeaderCollectionReusableView()
        let identifier = String(describing: FilterHeaderCollectionReusableView.self)
        if let header = collectionView.supplementaryView(forElementKind: kind, at: indexPath) as? FilterHeaderCollectionReusableView {
            headerView = header
        } else {
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath) as! FilterHeaderCollectionReusableView
        }
        headerView.fill(text: presenter.headerTitle, delegate: self)
        return headerView
    }

    // MARK: CollectionViewProtocols
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createItemCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 2, cellSpace: nil) ?? CGSize(width: 0, height: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        guard let product = presenter.getSelectedProduct(atIndex: indexPath) else {
            return
        }
        showPDPController(productEntity: product)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return createFilterHeaderView(collectionView: collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
    }
    
    // MARK: Navigation
    
    func showPDPController(productEntity: DetailProduct) {
        guard let controller = PDPControllerBuilder(transporter: Transporter(data: productEntity)).build() else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: ProductsPresenterDelegate

    func didFinishRequest() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.pullToRefresh.endRefreshing()
        }
    }
    
    func shouldShowError(withMessage message: String) {
        showDefaultSystemAlert(message: message) { [unowned self] _ in
            self.presenter.loadContent()
        }
    }
    
    // MARK: FilterSwitchDelegate
    
    func didTapSwitch(isOn: Bool) {
        presenter.loadFilteredContent(onlyOnSale: isOn)
    }

}
