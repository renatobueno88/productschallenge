//
//  ItemCollectionViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    let view: ItemCollectionCellView = ItemCollectionCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }
    
    func fill(dto: ItemDto) {
        view.fill(dto: dto)
    }
    
}
