//
//  ItemCollectionCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ItemDto {
    var imagePath = ""
    var name = ""
    var formattedPrice = NSAttributedString(string: "")
    var percentage: String = ""
    var oldPrice: NSAttributedString?
}

class ItemCollectionCellView: UIView {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceInstallmentsLabel: UILabel!
    @IBOutlet weak var promotionalPrice: UILabel!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        discountView.layer.cornerRadius = 6
    }
    
    func fill(dto: ItemDto) {
        let formattedImageUrl = dto.imagePath
        let placeholderImage = UIImage(named: "Amaro-logo-novo")
        productImageView.downloadImage(url: formattedImageUrl, placeholder: placeholderImage)
        nameLabel.text = dto.name
        priceInstallmentsLabel.attributedText = dto.formattedPrice
        discountView.isHidden = dto.percentage.isEmpty
        promotionalPrice.isHidden = dto.oldPrice == nil
        discountLabel.text = dto.percentage
        if let oldPrice = dto.oldPrice {
            promotionalPrice.attributedText = oldPrice
        }
    }
    

}
