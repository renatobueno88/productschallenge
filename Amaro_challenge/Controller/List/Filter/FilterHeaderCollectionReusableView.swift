//
//  FilterHeaderCollectionReusableView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class FilterHeaderCollectionReusableView: UICollectionReusableView {
    
    let view: FilterHeaderView = FilterHeaderView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.fillWithSubview(subview: view)
    }
    
    func fill(text: String, delegate: FilterSwitchDelegate?) {
        view.fill(text: text, delegate: delegate)
    }
    
}
