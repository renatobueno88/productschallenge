//
//  FilterHeaderView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 11/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol FilterSwitchDelegate: class {
    func didTapSwitch(isOn: Bool)
}

class FilterHeaderView: UIView {
    
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var filterSwitch: UISwitch!
    private weak var delegate: FilterSwitchDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(text: String, delegate: FilterSwitchDelegate?) {
        self.delegate = delegate
        filterLabel.text = text
    }
    
    @IBAction func switcherButtonAction(_ sender: UISwitch) {
        self.delegate?.didTapSwitch(isOn: sender.isOn)
    }
    
}
