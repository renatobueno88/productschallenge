//
//  ProductsInteractor.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 08/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ProductsInteractorDelegate: class {
    func didFinishRequest(products: [Product])
    func shouldShowError(error: Error?)
}

protocol ProductsInteractorProtocol {
    func fetchProducts()
}

class ProductsInteractor: ProductsInteractorProtocol {
    
    private var productsServiceType: ProductsService.Type
    private var productsRequest: ProductsService {
        return productsServiceType.init()
    }
    private weak var interactorDelegate: ProductsInteractorDelegate?
    
    init(productsServiceType: ProductsService.Type = ProductsRequest.self, interactorDelegate: ProductsInteractorDelegate?) {
        self.interactorDelegate = interactorDelegate
        self.productsServiceType = productsServiceType
    }
    
    // MARK: ProductsInteractorProtocol
    
    func fetchProducts() {
        productsRequest.fetchProducts { (products, error) in
            guard let products = products, error == nil, !products.isEmpty else {
                self.interactorDelegate?.shouldShowError(error: error)
                return
            }
            self.interactorDelegate?.didFinishRequest(products: products)
        }
    }
}
