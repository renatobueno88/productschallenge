//
//  ProductNamePriceTableViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ProductNamePriceTableViewCell: UITableViewCell {

    let view: ProductNamePriceCellView = ProductNamePriceCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func fill(name: String, priceAttributed: NSAttributedString)  {
        view.fill(name: name, priceAttributed: priceAttributed)
    }

}
