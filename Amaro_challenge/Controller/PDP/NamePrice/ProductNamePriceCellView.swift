//
//  ProductNamePriceCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ProductNamePriceCellView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(name: String, priceAttributed: NSAttributedString) {
        titleLabel.text = name
        priceLabel.attributedText = priceAttributed
    }

}
