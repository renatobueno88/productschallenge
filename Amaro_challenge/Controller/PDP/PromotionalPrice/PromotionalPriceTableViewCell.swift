//
//  PromotionalPriceTableViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class PromotionalPriceTableViewCell: UITableViewCell {

    let view: PromotionalPriceCellView = PromotionalPriceCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func fill(percentage: String, oldPrice: NSAttributedString?) {
        view.fill(percentage: percentage, oldPrice: oldPrice)
    }

}
