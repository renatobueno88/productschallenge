//
//  PromotionalPriceCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class PromotionalPriceCellView: UIView {

    @IBOutlet weak var promotionalPriceLabel: UILabel!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        discountView.layer.cornerRadius = 8
    }

    func fill(percentage: String, oldPrice: NSAttributedString?) {
        discountView.isHidden = percentage.isEmpty
        promotionalPriceLabel.isHidden = oldPrice == nil
        discountLabel.text = percentage
        if let oldPrice = oldPrice {
            promotionalPriceLabel.attributedText = oldPrice
        }
    }
    
    
}
