//
//  SizeCellPresenter.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol SizeLayoutSelectionDelegate: class {
    func shouldReloadCell(atIndex indexPath: [IndexPath])
}

class SizeCellPresenter {
    
    private (set)var sizes = [Sizes]()
    private (set)var selectedSizes = [String]()
    private var selectedIndexes = [IndexPath]()
    private weak var delegate: SizeLayoutSelectionDelegate?
    private weak var sizeDelegate: SizeSelectionDelegate?
    
    init(delegate: SizeLayoutSelectionDelegate?) {
        self.delegate = delegate
    }
    
    func fill(sizes: [Sizes], sizeDelegate: SizeSelectionDelegate?) {
        self.sizes = sizes
        self.sizeDelegate = sizeDelegate
    }

    // MARK: Cell Setup
    
    func numberOfSections() -> Int {
        return sizes.isEmpty ? 0 : 1
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return sizes.count
    }
    
    func didSelectSize(indexPath: IndexPath) {
        let size = sizes[indexPath.row]
        if !selectedIndexes.contains(indexPath) {
            selectedIndexes.append(indexPath)
        }
        if let index = selectedSizes.index(where: { $0 == size.size }) {
            selectedSizes.remove(at: index)
        } else {
            if !selectedSizes.isEmpty {
                selectedSizes.removeAll()
            }
            selectedSizes.append(size.size)
        }
        sizeDelegate?.didSelectSize(size: size)
        delegate?.shouldReloadCell(atIndex: selectedIndexes)
    }
}
