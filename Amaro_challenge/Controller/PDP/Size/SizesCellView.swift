//
//  SizesCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol SizeSelectionDelegate: class {
    func didSelectSize(size: Sizes)
}

class SizesCellView: UIView, CollectionViewProtocols, SizeLayoutSelectionDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var presenter = SizeCellPresenter(delegate: self)
    private weak var sizeDelegate: SizeSelectionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = false
        collectionView.registerCell(identifier: String(describing: SizeCollectionViewCell.self))
    }
    
    func fill(sizes: [Sizes], sizeDelegate: SizeSelectionDelegate?) {
        presenter.fill(sizes: sizes, sizeDelegate: sizeDelegate)
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    // MARK: Cell Creation
    
    func createCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SizeCollectionViewCell = SizeCollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        let size = presenter.sizes[indexPath.row].size
        let isSelected = presenter.selectedSizes.contains(size)
        cell.fill(size: size, isSelected: isSelected)
        return cell
    }
    
    // MARK: CollectionViewProtocols
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectSize(indexPath: indexPath)
    }
    
    // MARK: SizeLayoutSelectionDelegate
    
    func shouldReloadCell(atIndex indexPath: [IndexPath]) {
        DispatchQueue.main.async {
            self.collectionView.reloadItems(at: indexPath)
        }
    }

}
