//
//  SizesTableViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class SizesTableViewCell: UITableViewCell {

    let view: SizesCellView = SizesCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }
    
    func fill(sizes: [Sizes], sizeDelegate: SizeSelectionDelegate?) {
        view.fill(sizes: sizes, sizeDelegate: sizeDelegate)
    }

}
