//
//  SizeCollectionCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class SizeCollectionCellView: UIView {

    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sizeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sizeView.circleView(withBackgroundColor: .black)
    }
    
    func fill(size: String, isSelected: Bool) {
        sizeLabel.text = size
        setupSelection(isSelected: isSelected)
    }
    
    func setupSelection(isSelected: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.sizeView.backgroundColor = isSelected ? UIColor.lightGray : .black
            self.sizeLabel.textColor = isSelected ? UIColor.black : .white
        }
    }

}
