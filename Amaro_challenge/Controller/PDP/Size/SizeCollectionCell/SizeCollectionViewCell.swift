//
//  SizeCollectionViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class SizeCollectionViewCell: UICollectionViewCell {
    
    let view: SizeCollectionCellView = SizeCollectionCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }
    
    func fill(size: String, isSelected: Bool) {
        view.fill(size: size, isSelected: isSelected)
    }
    
}
