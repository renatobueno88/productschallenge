//
//  PDPPresenter.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol PDPPresenterDelegate: class {
    func addToCartFeedback(withSuccess success: Bool)
}

class PDPPresenter: TransporterProtocol, CellTypeSetupProtocol {
    
    private var productDetail = DetailProduct()
    var selectedSize: Sizes?
    private weak var delegate: PDPPresenterDelegate?
    private var cartLocalService = CartLocalServiceRequest(key: "UserCart")
    
    init(delegate: PDPPresenterDelegate?) {
        self.delegate = delegate
    }
    
    // MARK: CART ACTIONS
    
    func addToCard() {
        guard let selectedSize = selectedSize else {
            self.delegate?.addToCartFeedback(withSuccess: false)
            return
        }
        let cartItem = CartItem(productName: productDetail.productName,
                                productImage: productDetail.productImage,
                                currentPrice: productDetail.currentPrice,
                                priceInstallments: productDetail.priceInstallments.string,
                                selectedSize: selectedSize)
        saveCart(cartItem: cartItem)
        self.delegate?.addToCartFeedback(withSuccess: true)
    }
    
    func saveCart(cartItem: CartItem) {
        cartLocalService.saveLocalObject(cartObject: cartItem)
    }
    
    // MARK: TransporterProtocol
    
    func transporter(object: Transporter<Any>) {
        guard let productDetail = object.data as? DetailProduct else {
            return
        }
        self.productDetail = productDetail
        self.populateCells()
    }
    
    // MARK: Internal Validation methods
    
    private func shouldShowCell(fromType type: PDPCellType) -> Bool {
        switch type {
        case .image:
            return true
        case .namePrice:
            return !productDetail.productName.isEmpty && !productDetail.priceInstallments.string.isEmpty
        case .promotionalPrice:
            return productDetail.formattedDiscount != nil
        case .sizes:
            return !productDetail.sizes.isEmpty
        }
    }
    
    private func populateCells() {
        var cells: [PDPCellType] = [PDPCellType.image]
        if shouldShowCell(fromType: .namePrice) {
            cells.append(.namePrice)
        }
        if shouldShowCell(fromType: .promotionalPrice) {
            cells.append(.promotionalPrice)
        }
        if shouldShowCell(fromType: .sizes) {
            cells.append(.sizes)
        }
        self.cellTypes = cells
    }
    
    // MARK: CellTypeSetupProtocol
    
    typealias CellType = PDPCellType
    
    var cellTypes: [PDPCellType] = [PDPCellType]()
    
    func numberOfRows(inSection section: Int) -> Int {
        return cellTypes.count
    }
    
    func numberOfSections() -> Int {
        return cellTypes.isEmpty ? 0 : 1
    }
    
    func estimatedHeightForCell(indexPath: IndexPath) -> CGFloat {
        let cellType = getCellType(fromIndex: indexPath)
        return shouldShowCell(fromType: cellType) ? cellType.cellHeight : .leastNonzeroMagnitude
    }
    
    func getCellType(fromIndex indexPath: IndexPath) -> PDPCellType {
        return cellTypes[indexPath.row]
    }
  
    // MARK: DTO Setup
    
    func getProductImage() -> (imagePath: String, placeholder: UIImage) {
        return (imagePath: productDetail.productImage, placeholder: UIImage(named: "Amaro-logo-novo") ?? UIImage())
    }
    
    func getProductNamePrice() -> (productName: String, price: NSAttributedString) {
        return (productName: productDetail.productName, price: productDetail.priceInstallments)
    }
    
    func getPromotionalPrice() -> (percentage: String, oldPrice: NSAttributedString?) {
        guard let promotionalPrice = productDetail.formattedDiscount else {
            return (percentage: "", oldPrice: nil)
        } 
        return (percentage: promotionalPrice.percentage, oldPrice: promotionalPrice.oldPrice)
    }
    
    var getSizes: [Sizes] {
        return productDetail.sizes
    }
    
}
