//
//  PDPViewController.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 09/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

public typealias TableViewProtocols = UITableViewDelegate & UITableViewDataSource

class PDPViewController: UIViewController, TableViewProtocols, TransporterProtocol, SizeSelectionDelegate, PDPPresenterDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    lazy var presenter = PDPPresenter(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        setupCartButton()
    }
    
    private func setupCartButton() {
        navigationItem.rightBarButtonItem = setupCartOnNavigation()
    }
    
    override func addToCartAction() {
        presentCart()
    }
    
    // MARK: TransporterProtocol
    
    func transporter(object: Transporter<Any>) {
        presenter.transporter(object: object)
    }
    
    // MARK: Cell Creation
    
    func createImageCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductImageTableViewCell = ProductImageTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(imagePath: presenter.getProductImage().imagePath, placeHolder: presenter.getProductImage().placeholder)
        return cell
    }
    
    func createProductNameCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductNamePriceTableViewCell = ProductNamePriceTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(name: presenter.getProductNamePrice().productName, priceAttributed: presenter.getProductNamePrice().price)
        return cell
    }
    
    func createPromotionalCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: PromotionalPriceTableViewCell = PromotionalPriceTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(percentage: presenter.getPromotionalPrice().percentage, oldPrice: presenter.getPromotionalPrice().oldPrice)
        return cell
    }
    
    func createSizeCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: SizesTableViewCell = SizesTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(sizes: presenter.getSizes, sizeDelegate: self)
        return cell
    }
    
    // MARK: TableViewProtocols
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.getCellType(fromIndex: indexPath) {
        case .image:
            return createImageCell(tableView: tableView, indexPath: indexPath)
        case .namePrice:
            return createProductNameCell(tableView: tableView, indexPath: indexPath)
        case .promotionalPrice:
            return createPromotionalCell(tableView: tableView, indexPath: indexPath)
        case .sizes:
            return createSizeCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: SizeSelectionDelegate
    
    func didSelectSize(size: Sizes) {
        presenter.selectedSize = size
    }
    
    // MARK: PDPPresenterDelegate
    
    func addToCartFeedback(withSuccess success: Bool) {
        guard success else {
            self.showDefaultSystemAlert(message: "Por favor, selecione um tamanho.", actionTitle: "OK", cancelTitle: "", okActionHandler: nil)
            return
        }
        presentCart()
    }
    
    // MARK: Navigation
    
    func presentCart() {
        guard let controller = CartControllerBuilder().build() else {
            return
        }
        let embbedInNavigation = UINavigationController(rootViewController: controller)
        DispatchQueue.main.async {
            self.navigationController?.present(embbedInNavigation, animated: true, completion: nil)
        }
    }
    
    // MARK: AddToCard Action
    
    @IBAction func purchaseAction(_ sender: Any) {
        presenter.addToCard()
    }
    

}
