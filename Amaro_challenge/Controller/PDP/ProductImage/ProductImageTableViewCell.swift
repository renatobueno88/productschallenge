//
//  ProductImageTableViewCell.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ProductImageTableViewCell: UITableViewCell {

    let view: ProductImageCellView = ProductImageCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }
    
    func fill(imagePath: String, placeHolder: UIImage) {
        view.fill(imagePath: imagePath, placeHolder: placeHolder)
    }
}
