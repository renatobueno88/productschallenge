//
//  ProductImageCellView.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ProductImageCellView: UIView {

    @IBOutlet weak var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(imagePath: String, placeHolder: UIImage) {
        productImageView.downloadImage(url: imagePath, placeholder: placeHolder)
    }

}
