//
//  PDPCellType.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 14/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum PDPCellType: Int, CaseIterable {
    case image
    case namePrice
    case promotionalPrice
    case sizes
    
    var cellHeight: CGFloat {
        switch self {
        case .image, .namePrice, .promotionalPrice:
            return UITableView.automaticDimension
        case .sizes:
            return 68
        }
    }
}
