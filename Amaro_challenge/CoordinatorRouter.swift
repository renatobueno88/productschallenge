//
//  CoordinatorRouter.swift
//  Amaro_challenge
//
//  Created by Renato Souza Bueno on 10/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol CoordinatorRouter {
    associatedtype Controller: UIViewController
    var storyBoardName: String { get set }
    func build() -> UIViewController?
}

class PDPControllerBuilder: CoordinatorRouter {
    typealias Controller = PDPViewController
    
    var storyBoardName: String = "Main"
    private var productDetail: DetailProduct?
    
    init(transporter: Transporter<Any>) {
        productDetail = (transporter.data as? DetailProduct)
    }
    
    func build() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        let pdpController = storyboard.instantiateViewController(withIdentifier: identifier) as? Controller
        let product = productDetail ?? DetailProduct()
        pdpController?.transporter(object: Transporter(data: product))
        return pdpController
    }
}

class CartControllerBuilder: CoordinatorRouter {
    typealias Controller = CartViewController
    
    var storyBoardName: String = "Cart"
    
    init() {
    }
    
    func build() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        let cartController = storyboard.instantiateViewController(withIdentifier: identifier) as? Controller
        return cartController
    }
}


